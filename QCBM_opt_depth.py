"""Python script for finding optimal depth of the QCBM circuit for COBYLA."""

# Imports section

from qiskit import (
    QuantumCircuit,
    # QuantumRegister
)
from qiskit.circuit.library import (
    # EfficientSU2,
    TwoLocal,
)

# from qiskit.primitives import Sampler, BackendSampler
# from qiskit_aer import Aer, AerSimulator
from qiskit_aer.primitives import Sampler as AerSampler
from qiskit_aer.noise import NoiseModel

# from qiskit_algorithms.optimizers import ADAM, COBYLA
from qiskit_algorithms.utils import algorithm_globals
from qiskit_ibm_runtime import Sampler as IBMSampler
from qiskit_ibm_runtime import (
    Options,
    QiskitRuntimeService,
    IBMBackend,
    # Session
)
from qiskit_ibm_runtime.options import (
    TranspilationOptions,
    # SimulatorOptions
)
from qiskit_ibm_runtime.fake_provider import FakeProviderForBackendV2
from qiskit.transpiler import PassManager, Layout
from qiskit.transpiler.preset_passmanagers import generate_preset_pass_manager
from qiskit_ibm_runtime.transpiler.passes.scheduling import (
    ALAPScheduleAnalysis,
    PadDynamicalDecoupling,
)
from qiskit.circuit.library import XGate
import numpy as np

# import pandas as pd
import time
import os
import shutil
import datetime

# import configparser as cp
import tomlkit
import tomlkit.toml_file

# from scipy.stats import norm
# from scipy.optimize import minimize
from mmd_tools import mmd, rbf_kernel

# from process_data import dist, dist_pdf
# from costfun_tools import get_cost_function
from generate_prob_data import gen_prob_data
from cobyla_tools import train_cobylaV2, train_cobyla_ibmV2
from cmaes_tools import train_cmaes, train_cmaes_ibm
from adam_tools import train_adam, train_adam_ibm
from multishot import readlayoutmap, clones_to_layout


cfgkeys = (
    "cfg_version",
    "variant",
    "use_ibm_q",
    "ibm_backend",
    "num_qubits",
    "shots",
    "fake_backend",
    "noise",
    "use_gpu",
    "depth_min",
    "depth_max",
    "rhobeg",
    "max_iter",
    "layout_import",
    "depths",
    "cost_variant",
    "num_generations",
    "multi",
    "layout_file",
)


def print_dict_readable(d: dict):
    """Print a dictionary in a readable format.

    Args:
        d (dict): The dictionary to be printed

    Returns:
        None
    """
    for key, value in d.items():
        print(f"{key}: \n{value}\n")


def generate_unique_id():
    """Generate a unique ID based on the current time and a random number.

    Args:
        None

    Returns:
        str: The generated unique ID as a string
    """
    current_time = datetime.datetime.now()
    date_part = current_time.strftime("%Y%m%d_%H%M%S")
    random_int = os.urandom(4)  # Generate a random 4-byte string as an integer
    hash_part = hex(int.from_bytes(random_int, "big"))[
        2:
    ]  # Convert the integer to a hexadecimal string and remove the '0x' prefix
    unique_id = f"{date_part}_{hash_part}"

    return unique_id


def load_tomlconfig(
    filename: str, checkkeys: tuple[str] = None
) -> tomlkit.TOMLDocument:
    """Load a configuration file in TOML format and perform validation.

    Args:
        filename (str): The name of the TOML configuration file.
        checkkeys (tuple[str]): A tuple of strings representing the required
            keys in the configuration file.

    Returns:
        tomlkit.TOMLDocument: The parsed contents of the TOML configuration
        file as a TOMLDocument object.

    Raises:
        ValueError: If any required key is missing from the configuration file
            or if the "depths" key is present but both "depth_min" and
            "depth_max" are 0.
    """
    conftoml = tomlkit.toml_file.TOMLFile(filename).read()
    if checkkeys is not None and any(
        conftoml.get(key) is None for key in checkkeys
    ):
        raise ValueError("Missing config option.")
    if conftoml.get("depths") == 0:
        if conftoml.get("depth_min") == 0 or conftoml.get("depth_max") == 0:
            raise ValueError("Depth must be specified.")
        else:
            conftoml["depths"] = list(
                range(conftoml.get("depth_min"), conftoml.get("depth_max"))
            )
    else:
        if conftoml.get("depth_min") != 0 or conftoml.get("depth_max") != 0:
            raise ValueError("Depth cannot be ambiguous.")
    if conftoml.get("variant") not in ("cobyla", "cmaes", "adam"):
        raise ValueError("Invalid variant.")
    if conftoml.get("cost_variant") not in ("TV", "MMD", "MMD_torch"):
        raise ValueError("Invalid cost variant.")
    print("Loaded config:")
    print(conftoml)
    return conftoml


def create_datadir(
    datadirname: str = "experiment_data", archivename: str = None
) -> str:
    """Create a new directory for storing data files.

    Args:
        datadirname (str): The name of the directory to be created.
            Default is "experiment_data".
        archivename (str): The name of the subdirectory within the main
            directory to be created. Default is None, which generates
            a unique name based on the current time and a random number
            using the generate_unique_id() function.

    Returns:
        str: The path to the newly created directory.
    """
    # Generated are automatically archived in data directory
    # In case ./data does not exist create it.
    if not os.path.isdir(datadirname):
        os.mkdir(datadirname)
    # For each experiment we create a directory ./experiment_data/archive_name
    archivename = generate_unique_id()
    dirname = datadirname + "/" + archivename
    os.mkdir(dirname)
    print(f"Created directory {dirname}.")
    return dirname


def get_passmanager(
    backend: IBMBackend, layout: Layout | list[int]
) -> PassManager:
    """Create a new pass manager based on the given backend and layout.

    Args:
        backend (IBMBackend): The IBM backend to be used for the pass manager.
        layout (Layout | list[int]): The layout of the qubits on the backend.
            This can be either a Layout object or a list of integers
            representing the qubit indices in the desired order.

    Returns:
        PassManager: The newly created pass manager.
    """
    target = backend.target
    pm = generate_preset_pass_manager(
        target=target,
        optimization_level=1,  # up to 3 is better but takes longer time to run
        layout_method="trivial",  # 'trivial' can be used
        initial_layout=layout,
    )
    pm.scheduling = PassManager([
        # ALAPScheduleAnalysis(durations=target.durations()),
        ALAPScheduleAnalysis(durations=target.durations()),
        PadDynamicalDecoupling(
            durations=target.durations(),
            dd_sequences=[XGate(), XGate()],
            pulse_alignment=target.pulse_alignment,
        ),
    ])
    return pm


def get_dqc(cfg: tomlkit.TOMLDocument, depth: int) -> QuantumCircuit:
    """Create a quantum circuit with the given configuration and depth.

    Args:
        cfg (tomlkit.TOMLDocument): The TOML configuration file.
        depth (int): The desired depth of the quantum circuit.

    Returns:
        QuantumCircuit: The created quantum circuit.
    """
    nq = cfg.get("num_qubits")
    dqc = QuantumCircuit(nq, nq)
    dqc.h(dqc.qubits)
    # dansatz = EfficientSU2(
    #     nq,
    #     entanglement='circular',
    #     reps=depth
    # )
    dansatz = TwoLocal(
        num_qubits=nq,
        rotation_blocks="rz",
        entanglement_blocks="ecr",
        entanglement="circular",
        reps=depth,
    )
    dqc.compose(dansatz, inplace=True)
    dqc.measure(dqc.qubits, dqc.clbits)
    return dqc


def setup_mmd_instance(cfg: tomlkit.TOMLDocument) -> mmd:
    """
    Set up an instance of the Maximum Mean Discrepancy (MMD) with the given
    configuration.

    Args:
        cfg (tomlkit.TOMLDocument): The TOML configuration file.

    Returns:
        MMD: An instance of the MMD algorithm.
    """
    if any(cfg.get(opt) is None for opt in ("sigma_list", "basis", "kernel")):
        return mmd(
            sigma_list=[0.4, 8],
            basis=np.arange(2 ** cfg.get("num_qubits")),
            kernel=rbf_kernel,
        )
    else:
        # TODO implement alternative kernels
        return mmd(cfg.get("sigma_list"), cfg.get("basis"), cfg.get("kernel"))


def run_training(cfg: tomlkit.TOMLDocument, aux_cfg: dict) -> dict:
    """
    Run training for any valid variant.
    Args:
        cfg (tomlkit.TOMLDocument): The TOML configuration file.
        aux_cfg (dict): The auxiliary configuration dictionary.

    Returns:
        dict: A dictionary containing the results of training.
    """
    common_cfg = {
        # TOML part
        "max_iter": cfg.get("max_iter"),
        "shots": cfg.get("shots"),
        "rhobeg": cfg.get("rhobeg"),
        "cost_variant": cfg.get("cost_variant"),
        "num_qubits": cfg.get("num_qubits"),
        "multi": cfg.get("multi"),
        "layout_file": cfg.get("layout_file"),
        "initial_params": cfg.get("initial_params"),
        # AUX part
        "prob_data": aux_cfg.get("prob_data"),
        "circuit": aux_cfg.get("qc"),
        "backend": aux_cfg.get("backend"),
        "maximmum": aux_cfg.get("maximmum"),
        "pm": aux_cfg.get("passmanager"),
    }
    match cfg.get("cost_variant"):
        case "MMD":
            common_cfg.update({"mmd_instance": setup_mmd_instance(cfg)})
        case _:
            common_cfg.update({"mmd_instance": None})
            raise ValueError("TV numerical gradient not implemented.")
    if cfg.get("use_ibm_q"):
        common_cfg.update({"service": aux_cfg.get("service")})
    else:
        common_cfg.update({"sampler": aux_cfg.get("sampler")})
    match cfg.get("variant"):
        case "cobyla":
            cobyla_cfg = common_cfg.copy()
            if cfg.get("use_ibm_q"):
                dres_dict = train_cobyla_ibmV2(cfg=cobyla_cfg)
            else:
                dres_dict = train_cobylaV2(cfg=cobyla_cfg)
        case "cmaes":
            cma_cfg = common_cfg.copy()
            cma_cfg.update({"num_generations": cfg.get("num_generations")})
            if cfg.get("use_ibm_q"):
                dres_dict = train_cmaes_ibm(cfg=cma_cfg)
            else:
                dres_dict = train_cmaes(cfg=cma_cfg)
        case "adam":
            adam_cfg = common_cfg.copy()
            adam_cfg.update({"num_generations": cfg.get("num_generations")})
            if cfg.get("use_ibm_q"):
                dres_dict = train_adam_ibm(cfg=adam_cfg)
            else:
                dres_dict = train_adam(cfg=adam_cfg)
        case _:
            raise ValueError("Invalid algorithms variant.")
    return dres_dict


def loop_runner(config: tomlkit.TOMLDocument, aux_config: dict):
    """
    Run training for depths and save data.

    Args:
        config (tomlkit.TOMLDocument): Experiment configuration file.
        aux_config (dict): Dictionary of auxiliary configuration data.

    Returns:
        None
    """
    for depth in config.get("depths"):
        variant = config.get("variant")
        print(f"Running experiment variant {variant}. Depth set to {depth}.")
        if config.get("multi"):
            aux_config.update({"qc": get_dqc(cfg=config, depth=depth)})
        else:
            aux_config.update({
                "qc": aux_config.get("passmanager").run(
                    get_dqc(cfg=config, depth=depth)
                )
            })
        dres_dict = run_training(config=config, aux_config=aux_config)
        savedata(
            config.get("variant") + "_D" + str(depth),
            dirname=aux_config.get("dirname"),
            config=config,
            res_dict=dres_dict,
        )


def savedata(
    expname: str, dirname: str, config: tomlkit.TOMLDocument, res_dict: dict
):
    """Save the results of an experiment in a directory.

    Args:
        expname (str): The name of the experiment.
        dirname (str): The directory where the experiment results will
            be saved.
        config (tomlkit.TOMLDocument): The configuration file for the
            experiment.
        res_dict (dict): A dictionary containing the results of the experiment.

    Returns:
        None
    """
    save_prefix = (
        dirname
        + "/"
        + word_join((
            config.get("variant"),
            "Ver",
            config.get("cfg_version"),
            expname,
            time.strftime("%Y%m%d-%H%M%S"),
        ))
    )
    for key, value in res_dict.items():
        print(key)
        print(value)
        np.savetxt(save_prefix + "-" + key + ".txt", value)


def word_join(t: tuple, separator: str = "-") -> str:
    """Safely join tuple which can be casted to string."""
    return separator.join((str(s) for s in t))


def setup_online_experiment(cfg: dict, aux: dict) -> Options:
    """Set up the online experiment using Qiskit Runtime.

    Args:
        cfg (dict): The configuration dictionary for the online experiment.
            It should contain the following keys:
                - "account": The name of the IBM Quantum account to use for
                    the experiment. If not provided, it defaults to the
                    default account.
                - "ibm_backend": The name of the IBM backend to use for the
                    experiment.

    Returns:
        Options: The updated configuration dictionary with the "options" key
            added.
    """
    account = cfg.get("account")
    if account is None or account == 0:  # Use default account
        aux.update({"service": QiskitRuntimeService()})
    else:
        aux.update({"service": QiskitRuntimeService(name=account)})
    aux.update(
        {"backend": aux.get("service").get_backend(cfg.get("ibm_backend"))}
    )
    options = Options(
        # optimization_level=3,
        transpilation=TranspilationOptions(skip_transpilation=True)
    )
    cfg.update({"options": options})
    return options


def setup_offline_experiment(cfg: dict, aux: dict):
    """Set up the offline experiment using Qiskit Runtime.

    Args:
        cfg (dict): The configuration dictionary for the offline experiment.
            It should contain the following keys:
                - "account": The name of the IBM Quantum account to use for
                    the experiment. If not provided, it defaults to the
                    default account.
                - "ibm_backend": The name of the IBM backend to use for the
                    experiment.

    Returns:
        None
    """
    account = cfg.get("account")
    if account is None or account == 0:
        aux.update({
            "backend": FakeProviderForBackendV2().backend(
                name=cfg.get("fake_backend")
            )
        })
    else:
        service = QiskitRuntimeService(name=account)
        aux.update({"backend": service.get_backend(cfg.get("ibm_backend"))})
    if cfg.get("noise"):
        noise_model = NoiseModel.from_backend(aux.get("backend"))
        aer_backend_options = {
            "noise_model": noise_model,
            "seed_simulator": algorithm_globals.random_seed,
            "method": "density_matrix",
        }
        if cfg.get("use_gpu"):
            options = ({"device": "GPU"}, {"batched_shots_gpu": True})
        else:
            options = ({"device": "CPU"},)
        for opt in options:
            aer_backend_options.update(opt)
        aux.update({
            "sampler": AerSampler(
                backend_options=aer_backend_options,
                skip_transpilation=True,
            )
        })
    else:
        aux.update({
            "backend": FakeProviderForBackendV2().backend(
                name=cfg.get("fake_backend")
            )
        })
        options = Options(
            transpilation=TranspilationOptions(skip_transpilation=True)
        )
        aux.update({
            "sampler": IBMSampler(backend=aux.get("backend"), options=options)
        })


def main():
    """Run main function of the COBYLA training script."""
    # -----------------
    # | Setup section |
    # -----------------
    #
    conffilename = "QCBM_opt_depth.toml"
    cfg = load_tomlconfig(conffilename, cfgkeys)

    num_discrete_values = 2 ** cfg.get("num_qubits")
    maximmum = int(cfg.get("num_qubits") * "1", 2) + 1
    # variant_prefix = cfg.get("variant")[0:3].capitalize()
    # Create experiment directory
    dirname = create_datadir(
        datadirname="data",
        archivename=word_join((
            time.strftime("%Y%m%d-%H%M%S"),
            cfg.get("variant"),
            "Ver",
            cfg.get("cfg_version"),
        )),
    )
    # Create file prefix for the data files
    file_prefix = (
        dirname
        + "/"
        + word_join((cfg.get("variant"), "Ver", cfg.get("cfg_version")))
    )
    # Filename for the probability data file
    prob_data_filename = word_join((file_prefix, "prob_data.txt"))
    # Prepare probability distribution data
    # prob_data is saved by gen_prob_data
    prob_data = gen_prob_data(
        num_discrete_values=num_discrete_values, filename=prob_data_filename
    )
    # Save cobyla_cfg.toml and prob generator
    for f in (
        "QCBM_opt_depth.toml",
        "generate_prob_data.py",
        "QCBM_opt_depth.sh",
    ):
        try:
            shutil.copy(f, word_join((file_prefix, f)))
        except FileNotFoundError:
            print(f"WARNING: File {f} not found, no backup make.")
    aux = {
        "prob_data": prob_data,
        "maximmum": maximmum,
        "dirname": dirname,
    }
    # JUNCTION run simulation or quantum computer
    if cfg.get("use_ibm_q"):
        setup_online_experiment(cfg=cfg, aux=aux)
    else:
        setup_offline_experiment(cfg=cfg, aux=aux)
    # JUNCTION if multi qubit is used - multishot
    multi = cfg.get("multi")
    if multi:
        multilayout = clones_to_layout(
            readlayoutmap(
                filename=cfg.get("layout_file"),
            )[0:multi]
        )
        print(f"multilayout: {multilayout}")
        aux.update({
            "passmanager": get_passmanager(
                backend=aux.get("backend"), layout=multilayout
            ),
            "multilayout": multilayout,
        })
    else:
        aux.update({
            "passmanager": get_passmanager(
                backend=aux.get("backend"), layout=cfg.get("layout_import")
            )
        })

    # execute the experiment loop
    loop_runner(config=cfg, aux_config=aux)


if __name__ == "__main__":
    start_time = time.time()
    main()
    print("--- %s seconds ---" % (time.time() - start_time))
