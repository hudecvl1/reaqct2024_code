#!/bin/bash
### Job Name
#PBS -N QCBM_opt_depth_cob

### Send email on abort or end
#PBS -m ae
#PBS -M hudecvl1@fjfi.cvut.cz

### required runtime 
#PBS -l walltime=01:00:00 
### queue for submission 
#PBS -q student 

### Merge output and error files 
#PBS -j oe 
 
### Request 16 GB of memory and 1 CPU core on 1 compute node 
#PBS -l select=1:mem=16G:ncpus=1 

cd $PBS_O_WORKDIR

source venv/bin/activate

echo "This is a test." >> test.txt
