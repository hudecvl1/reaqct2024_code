# update system
# apt update
# apt upgrade -y
# install Linux tools and Python 3
# apt install -y software-properties-common wget curl python3-dev python3-pip python3-wheel python3-setuptools
# install Python packages
python3 -m pip install --upgrade pip
pip3 install -r .devcontainer/requirements.txt
# install additional recommended packages
# apt install -y zlib1g g++ freeglut3-dev libx11-dev libxmu-dev libxi-dev libglu1-mesa libglu1-mesa-dev libfreeimage-dev
