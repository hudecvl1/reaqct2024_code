"""Tools for QCBM training with COBYLA."""

# from costfun_tools import (
#     get_cost_function,
#     get_gradient_analyticV2,
#     get_gradient_analyticV2_torch
# )
from costfun_tools import (
    get_init_params,
    get_tol,
    get_amsgrad,
    get_cost_functionV2,
    get_gradient_analytic_function,
)

# from mmd_tools import mmd, rbf_kernel
# from scipy.optimize import minimize
from qiskit_algorithms.optimizers import ADAM
from qiskit_ibm_runtime import SamplerV2 as IBMSampler
from qiskit_ibm_runtime import (
    Batch,
    # Session,
    # Options,
    # QiskitRuntimeService
)
import traceback


def train_adam(cfg: dict) -> dict:
    """Train a QCBM using the Adam optimizer.

    Args:
        cfg (dict): Configuration dictionary for training.

    Returns:
        dict: Dictionary containing the trained parameters, cost history, etc.
    """
    parameters = get_init_params(cfg)
    amsgrad = get_amsgrad(cfg)
    optimizer = ADAM(maxiter=cfg.get("max_iter"), amsgrad=amsgrad)
    prob_history = []
    grad_history = []
    par_history = []
    cost_fun = get_cost_functionV2(cfg=cfg)
    if cfg.get("cost_variant") == "TV":
        print("TV cost: using numeric gradient")
        gradient = None
    else:
        gradient = get_gradient_analytic_function(
            cfg=cfg,
            prob_history=prob_history,
            grad_history=grad_history,
            par_history=par_history,
        )
    optimizer_result = optimizer.minimize(
        fun=cost_fun,
        x0=parameters,
        jac=gradient,
    )
    # Result dictionary, does not contain cost history
    # unless we use the numeric gradient
    res_dict = {
        "x": optimizer_result.x,  # Solution parameters, [float, ...]
        "fun": [optimizer_result.fun],  # Solution cost, float
        "nfev": [optimizer_result.nfev],  # Number of iterations, int
        "par_hist": par_history,
        "prob_history": prob_history,  # History of probabilities
        "grad_history": grad_history,
    }
    return res_dict


def train_adam_ibm(cfg: dict):
    """Train a QCBM using the Adam optimizer on IBM hardware.

    Args:
        cfg (dict): Configuration dictionary for training.

    Returns:
        dict: Dictionary containing the trained parameters, cost history, etc.
    """
    parameters = get_init_params(cfg=cfg)
    amsgrad = get_amsgrad(cfg=cfg)
    optimizer = ADAM(maxiter=cfg.get("max_iter"), amsgrad=amsgrad)
    prob_history = []
    grad_history = []
    par_history = []
    with Batch(service=cfg.service, backend=cfg.backend) as batch:
        try:
            ibmsampler = IBMSampler(batch, options=cfg["options"])
            cfg.update({"sampler": ibmsampler})
            if cfg.get("cost_variant") == "TV":
                print("TV cost: using numeric gradient")
                gradient = None
            else:
                gradient = get_gradient_analytic_function(
                    cfg=cfg,
                    prob_history=prob_history,
                    grad_history=grad_history,
                    par_history=par_history,
                )
            cost_fun = get_cost_functionV2(cfg=cfg)
            cfg.update({"sampler": ibmsampler})
            optimizer_result = optimizer.minimize(
                fun=cost_fun,
                x0=parameters,
                jac=gradient,
            )
        except Exception as e:
            # cancel job if exception occurs
            print(f"Failed: {e}")
            print(traceback.format_exc())
            batch.cancel()
        # clear reference to sampler to free up resources with GC
        cfg.update({"sampler": None})
        # close batch on successful completion
        batch.close()
    res_dict = {
        "x": optimizer_result.x,
        "fun": optimizer_result.fun,
        "nfev": optimizer_result.nfev,
        "par_hist": par_history,
        "prob_history": prob_history,
        "grad_history": grad_history,
    }
    return res_dict


def train_adamV2(cfg: dict, ibm: bool) -> dict:
    """Train a QCBM using the Adam optimizer.

    Args:
        cfg (dict): Configuration dictionary for training.

    Returns:
        dict: Dictionary containing the trained parameters, cost history, etc.
    """

    def minimize_subroutine(cfg: dict) -> dict:
        parameters = get_init_params(cfg)
        amsgrad = get_amsgrad(cfg)
        optimizer = ADAM(maxiter=cfg.get("max_iter"), amsgrad=amsgrad)
        prob_history = []
        grad_history = []
        par_history = []
        cost_fun = get_cost_functionV2(cfg=cfg)
        if cfg.get("cost_variant") == "TV":
            print("TV cost: using numeric gradient")
            gradient = None
        else:
            gradient = get_gradient_analytic_function(
                cfg=cfg,
                prob_history=prob_history,
                grad_history=grad_history,
                par_history=par_history,
            )
        optimizer_result = optimizer.minimize(
            fun=cost_fun,
            x0=parameters,
            jac=gradient,
            tol=get_tol(cfg),
        )
        # Result dictionary, does not contain cost history
        # unless we use the numeric gradient
        res_dict = {
            "x": optimizer_result.x,  # Solution parameters, [float, ...]
            "fun": [optimizer_result.fun],  # Solution cost, float
            "nfev": [optimizer_result.nfev],  # Number of iterations, int
            "par_hist": par_history,
            "prob_history": prob_history,  # History of probabilities
            "grad_history": grad_history,
        }
        return res_dict

    if ibm:
        with Batch(service=cfg.service, backend=cfg.backend) as batch:
            print(f"batch: {batch}")
            try:
                ibmsampler = IBMSampler(batch, options=cfg["options"])
                cfg.update({"sampler": ibmsampler})
                res_dict = minimize_subroutine(cfg=cfg)
            except Exception as e:
                # cancel job if exception occurs
                print(f"Failed: {e}")
                print(traceback.format_exc())
                batch.cancel()
            # clear reference to sampler to free up resources with GC
            cfg.update({"sampler": None})
            # close batch on successful completion
            batch.close()
    else:
        res_dict = minimize_subroutine(cfg=cfg)
    return res_dict
