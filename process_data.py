"""Data processing module."""

import numpy as np
import pandas as pd
from qiskit.result import ProbDistribution


def dist(sampler, circuit, parameters):
    """Execute circuit with parameters on sampler, return nearest PD."""
    job = sampler.run(circuits=circuit, parameter_values=parameters)
    result = job.result()
    quasi_dist = result.quasi_dists[0]
    prob_dist = quasi_dist.nearest_probability_distribution()
    return prob_dist


def dist_pdf(dist, maximmum):
    """Convert distribuiton to PDF."""
    prob = []
    for key in range(0, maximmum):
        value = dist.get(key)
        if value:
            prob.append(value)
        else:
            prob.append(0)
    return np.array(prob)


def pd_process(data_df):
    """Get mean, standard deviation and standard error over mean."""
    res_proc = pd.DataFrame({
        "mean": data_df.mean(axis=1),
        "std": data_df.std(axis=1),
        "sem": data_df.sem(axis=1),
    })
    return res_proc


def bin_mean(data, bin_size):
    """Bin by averaging data with bin_size."""
    if len(data) % bin_size:
        raise ValueError
    bin_data = []
    for i in range(0, len(data), bin_size):
        bin_data.append(np.mean(data[i : i + bin_size]))
    return bin_data


def counts_to_pd(counts: dict) -> ProbDistribution:
    """Get probability distribution from counts."""
    combined_shots = np.sum(list(counts.values()))
    pd_from_shots = {}
    for key, value in counts.items():
        pd_from_shots.update({key: float(value) / combined_shots})
    return ProbDistribution(data=pd_from_shots, shots=combined_shots)
