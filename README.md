# ReAQCT2024_Code

This repository contains the code used for the experiment with machine learning on Quantum Circuit Born Machine.

The experiment uses gradient based and gradient free learning methods and comapares their results in learning
a benchmark distribution. From the results of the learning we can observe the some properties of the learing
process for this generative method.