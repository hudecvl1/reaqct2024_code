"""Generate probability data."""

import numpy as np
import scipy


def gen_prob_data(num_discrete_values, filename=None) -> np.ndarray:
    """Generate probability data for a given number of discrete values.

    Args:
        num_discrete_values (int): Number of discrete values to generate
            probability data for.
        filename (str, optional): If provided, save the generated probability
            data to a file with this name. Defaults to None.

    Returns:
        np.ndarray: Generated probability data.
    """
    coords = np.linspace(-4, 4, num_discrete_values)
    # Simple distribution
    # prob_data = normal.pdf(coords)
    prob_data = (
        0.4 * scipy.stats.norm(loc=-3, scale=0.3).pdf(coords)
        + 0.3 * scipy.stats.norm(loc=1, scale=0.3).pdf(coords)
        + 0.2 * scipy.stats.norm(loc=3, scale=0.6).pdf(coords)
        + 0.1 * scipy.stats.norm(loc=-1, scale=0.6).pdf(coords)
    )
    # Normalize
    prob_data = prob_data / np.sum(prob_data)
    if filename is not None:
        np.savetxt(filename, prob_data)
    return prob_data
