# %%
import datetime
import uuid
import os
from pathlib import Path
import tarfile
from qiskit_ibm_runtime import QiskitRuntimeService
import json
from qiskit_ibm_runtime import RuntimeEncoder


# %%
# Progress Bar
def progress(
    iteration, total, prefix="", suffix="", decimals=1, length=100, fill="█"
):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + "-" * (length - filledLength)
    print(
        "\r%s |%s| %s%% %s"
        % (prefix, bar, str(round(iteration / total * 100, decimals)), suffix),
        end="\r",
    )
    # Print New Line on Complete
    if iteration == total:
        print()


# %%
def save_as_targz(files, filename):
    """
    Saves a list of files as a tar file
    @params:
        files       - Required   : List of files to be saved (List)
        filename    - Required   : Name for the tar file (Str)
    """
    with tarfile.open(filename, "w:gz") as tar:
        for f in files:
            if os.path.isfile(f):
                tar.add(f, arcname=os.path.basename(f))
            elif os.path.isdir(f):
                tar.add(f, arcname=os.path.basename(f), recursive=True)


# %%
def get_id(prefix: str = None, sufix: str = None):
    """
    Generates a unique ID based on current timestamp and a UUID
    @params:
        prefix       - Optional  : prefix string (Str)
        sufix      - Optional  : suffix string (Str)
    """
    if prefix is None:
        prefix = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    if sufix is None:
        sufix = str(uuid.uuid4())
    return f"{prefix}_{sufix}"


# %%
def create_directory(name):
    directory = name
    if not Path(directory).exists():
        os.makedirs(directory)


# %%
# Remove a file on directory function
def remove_file(directory, filename):
    try:
        os.remove(os.path.join(directory, filename))
    except FileNotFoundError:
        print(f"File {filename} not found in directory {directory}")


# %%
def main():
    # Setup
    # initialize Qiskit Runtime Service
    print("Setting up IBM Quantum Instance service.")
    service = QiskitRuntimeService()
    # In case you are using multiple accounts uncomment the line below
    # and pass your account name
    # service = QiskitRuntimeService(name="account_name")

    # Retrieve jobs from IBM Quantum Instance over the last year
    print("Retrieving jobs from IBM Quantum Instance")
    jobs_history = service.jobs(
        created_after=datetime.datetime.now() - datetime.timedelta(days=365)
    )

    # Process jobs and save as JSON files
    print("Processing jobs")
    files = []
    total = len(jobs_history)  # Total iterations
    for i, job in enumerate(jobs_history):
        if not job.running():
            d = {
                "job_id": job.job_id(),
                "status": str(job.status()),
                "backend": job.backend().name,
                "properties": job.properties().to_dict(),
                "metrics": job.metrics(),
            }
        filename = get_id(prefix=str(i) + d["job_id"]) + ".json"
        files.append(filename)
        with open(filename, "w") as file:
            json.dump(d, file, cls=RuntimeEncoder)
        progress(i + 1, total)

    # Save all JSON files as a tar file
    print("Saving all JSON files as a tar file")
    save_as_targz(files, "data.tar.gz")

    # Remove all JSON files
    print("Removing all JSON files")
    for f in files:
        remove_file(".", f)

    # Print finish message
    print("All data saved to archive data.tar.gz.")
    print(
        " You can close the program and send the archive to the Quantum"
        " Instance administrator."
    )


# %%
if __name__ == "__main__":
    main()
