"""Maximmum Mean Discrepancy module."""

import numpy as np
import torch


def rbf_kernel(x, y, sigma_list):
    """Return an RBF kernel for vectors x and y and sums over sigmas."""
    exponent = np.abs(x[:, None] - y[None, :]) ** 2
    K = 0.0
    for sigma in sigma_list:
        gamma = 1.0 / (2 * sigma)
        K = K + np.exp(-gamma * exponent)
    return K


class mmd:
    """Class for the Maximmum Mean Discrepancy with methods."""

    def __init__(self, sigma_list, basis, kernel):
        """Init function of the MMD class."""
        self.sigma_list = sigma_list
        self.basis = basis
        self.kernel = kernel
        self.K = self.kernel(basis, basis, self.sigma_list)

    def calc(self, px, py):
        """Return kernel expval for difference of two vectors."""
        pxy = px - py
        return self.kernel_expect(pxy, pxy)

    def kernel_expect(self, px, py):
        """Return kernel expectation value for two vectors."""
        return px.dot(self.K).dot(py)


def torch_mmd(x, y, kernel, device):
    """Calculate empirical maximum mean discrepancy.

       The lower the result the more evidence that
       distributions are the same.

    Args:
        x: first sample, distribution P
        y: second sample, distribution Q
        kernel: kernel type such as "multiscale" or "rbf"
    """
    xx, yy, zz = torch.mm(x, x.t()), torch.mm(y, y.t()), torch.mm(x, y.t())
    rx = xx.diag().unsqueeze(0).expand_as(xx)
    ry = yy.diag().unsqueeze(0).expand_as(yy)

    dxx = rx.t() + rx - 2.0 * xx  # Used for A in (1)
    dyy = ry.t() + ry - 2.0 * yy  # Used for B in (1)
    dxy = rx.t() + ry - 2.0 * zz  # Used for C in (1)

    XX, YY, XY = (
        torch.zeros(xx.shape).to(device),
        torch.zeros(xx.shape).to(device),
        torch.zeros(xx.shape).to(device),
    )

    if kernel == "multiscale":

        bandwidth_range = [0.2, 0.5, 0.9, 1.3]
        for a in bandwidth_range:
            XX += a**2 * (a**2 + dxx) ** -1
            YY += a**2 * (a**2 + dyy) ** -1
            XY += a**2 * (a**2 + dxy) ** -1

    if kernel == "rbf":

        bandwidth_range = [10, 15, 20, 50]
        for a in bandwidth_range:
            XX += torch.exp(-0.5 * dxx / a)
            YY += torch.exp(-0.5 * dyy / a)
            XY += torch.exp(-0.5 * dxy / a)

    return torch.mean(XX + YY - 2.0 * XY)
