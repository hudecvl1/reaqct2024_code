"""Tools for QCBM training with COBYLA."""

import numpy as np
from costfun_tools import (
    get_cost_functionV2,
    get_init_params,
    flatten_list,
)
import traceback

# from mmd_tools import mmd, rbf_kernel
# from scipy.optimize import minimize
from qiskit_ibm_runtime import SamplerV2 as IBMSampler
from qiskit_ibm_runtime import (
    Batch,
    # Session,
    # Options,
    # QiskitRuntimeService
)
from cmaes import CMA


def train_cmaes(cfg: dict) -> dict:
    """Train QCBM with CMA-ES on simulator."""
    parameters = get_init_params(cfg=cfg)
    sigma = 0.3
    bounds = np.array(
        [[-np.pi / 2, np.pi / 2] for _ in range(len(parameters))]
    )
    # initialize CMA without warmstart
    # CMA-ES does not have tolerance parameter
    cma_optimizer = CMA(
        mean=parameters,
        sigma=sigma,
        bounds=bounds,
    )
    # create empty lists to store cost and parameter values
    cost_history = []
    parameter_history = []

    # get number of generations from config file
    num_generations = cfg.get("num_generations")

    # get cost function from costfun_tools
    cost_fun = get_cost_functionV2(cfg=cfg)

    # initialize generation counter
    generation = 1

    while generation <= num_generations:
        population = []
        pop_size = cma_optimizer.population_size
        print(f"Starting generation {generation} with popsize {pop_size}.")
        for _ in range(pop_size):
            x = cma_optimizer.ask()
            population.append(x)
        parameter_history.append(population)
        pop_cost = [cost_fun(x) for x in population]
        cost_history.append(pop_cost)
        cma_optimizer.tell(list(zip(population, pop_cost)))
        if cma_optimizer.should_stop():
            break
        generation += 1
    # return optimizer_result, cost_history, callback_dict
    fun = np.min(cost_history[-1])
    # Get the best parameters and minimum cost from the last generation
    x = parameter_history[-1][np.argmin(cost_history[-1])]
    # Calculate the minimum cost for each previous generation
    ch = [np.min(cost_history[g]) for g in range(generation)]
    # Get the best parameters for each previous generation
    ph = [
        parameter_history[g][np.argmin(cost_history[g])]
        for g in range(generation)
    ]
    res_dict = {
        "x": x,  # Solution parameters, [float, ...]
        "fun": [fun],  # Solution cost, best from last generation, float
        "nfev": [len(flatten_list(cost_history))],  # Number of iterations, int
        "iters": [generation],  # Number of actual generations, int
        "par_hist": ph,
        "cost_history": ch,
    }
    return res_dict


def train_cmaes_ibm(cfg: dict) -> dict:
    """Train QCBM with CMAES."""
    parameters = get_init_params(cfg=cfg)
    sigma = 0.3
    bounds = np.array(
        [[-np.pi / 2, np.pi / 2] for _ in range(len(parameters))]
    )
    # initialize CMA without warmstart
    cma_optimizer = CMA(
        mean=parameters,
        sigma=sigma,
        bounds=bounds,
    )
    cost_history = []
    parameter_history = []
    num_generations = cfg.get("num_generations")
    cost_fun = get_cost_functionV2(cfg=cfg)
    generation = 0
    with Batch(service=cfg.service, backend=cfg.backend) as batch:
        # COBYLA is not parallel => use Session instead of Batch.
        try:
            ibmsampler = IBMSampler(batch, options=cfg["options"])
            cfg.update({"sampler": ibmsampler})
            cost_fun = get_cost_functionV2(cfg=cfg, cost_history=cost_history)
            while generation < num_generations:
                population = []
                pop_size = cma_optimizer.population_size
                for _ in range(pop_size):
                    x = cma_optimizer.ask()
                    population.append(x)
                    # parameter_history.append(x)
                parameter_history.append(population)
                pop_cost = [cost_fun(x) for x in population]
                cost_history.append(pop_cost)
                cma_optimizer.tell(list(zip(population, pop_cost)))
                if cma_optimizer.should_stop():
                    break
                generation += 1
        except Exception as e:
            # close batch if exception occurs
            print(f"Failed: {e}")
            print(traceback.format_exc())
            batch.cancel()
        # clear reference to sampler to free up resources with GC
        cfg.update({"sampler": None})
        # close batch on successful completion
        batch.close()
    # return optimizer_result, cost_history, callback_dict
    fun = np.min(cost_history[-1])
    x = parameter_history[-1][np.argmin(cost_history[-1])]
    ch = [np.min(cost_history[g]) for g in range(generation)]
    ph = [
        parameter_history[g][np.argmin(cost_history[g])]
        for g in range(generation)
    ]
    res_dict = {
        "x": x,  # Solution parameters, [float, ...]
        "fun": [fun],  # Solution cost, best from last generation, float
        "nfev": [len(flatten_list(cost_history))],  # Number of iterations, int
        "iters": [generation],  # Number of actual generations, int
        "par_hist": ph,
        "cost_history": ch,
    }
    return res_dict
