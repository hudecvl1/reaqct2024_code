"""Cost function tools module."""

import numpy as np
import torch
from mmd_tools import (
    # mmd,
    # rbf_kernel,
    torch_mmd,
)
from process_data import (
    # dist,
    dist_pdf,
    counts_to_pd,
)
from qiskit import QuantumCircuit

# from qiskit.primitives import Sampler
from qiskit_ibm_runtime import SamplerV1, SamplerV2
from qiskit.result import ProbDistribution
from multishot import multirunV2, readlayoutmap


def get_init_params(cfg: dict) -> list[float]:
    """Returns the parameters of a quantum circuit from cfg argument.

    Args:
        cfg (dict): A dictionary containing the initial parameters.

    Returns:
        list[float]: The initialized parameters of the quantum circuit.
    """
    if cfg.get("init_params") is None:  # init_params is None -> use zeros
        parameters = np.zeros(cfg.get("circuit").num_parameters)
    elif cfg.get("init_params") == "rand":  # for 'rand' value generate random
        parameters = np.random.rand(cfg.get("circuit").num_parameters) * np.pi
    else:
        parameters = cfg.get("init_params")

    return parameters


def get_tol(cfg: dict) -> float:
    """Returns the tolerance value for optimization from cfg argument.

    Args:
        cfg (dict): A dictionary containing the initial tolerance value.

    Returns:
        float: The initialized tolerance value for optimization.
    """
    if cfg["tol"] is None:
        # If tol is None -> use default value
        return 1e-6
    else:
        return cfg["tol"]


def flatten_list(lst: list[list]) -> list:
    """Flattens a list of lists into a single list.

    Args:
        lst (list[list]): The list to flatten.

    Returns:
        list: The flattened list.
    """
    flat_list = []
    for item in lst:
        if isinstance(item, list):
            flat_list.extend(flatten_list(item))
        else:
            flat_list.append(item)
    return flat_list


def get_amsgrad(cfg: dict) -> bool:
    """
    Returns "amsgrad" key value from cfg argument or default value.

    If the "amsgrad" key is not present in the configuration,
    this function returns True by default.

    Args:
        cfg (dict): The configuration dictionary searched for "amsgrad" key.

    Returns:
        bool: The value of the "amsgrad" key if it exists, otherwise True.
    """
    if cfg["amsgrad"] is None:
        # If amsgrad is None -> use default value
        return True
    else:
        return cfg["amsgrad"]


def get_pdV2(
    circuit: QuantumCircuit,
    parameter_values: list[float, ...],
    sampler: SamplerV1,
    shots: int,
    aux: dict = None,
) -> ProbDistribution:
    """
    Use SamplerV2 to get ProbDistribution.

    Args:
        circuit (QuantumCircuit): The quantum circuit to execute.
        parameter_values (list[float, ...]): The values of the parameters to
            use for the circuit.
        sampler (SamplerV1): The sampler to use for executing the circuit.
        shots (int): The number of shots to take for each execution of the
            circuit. If None or 0, the function will return a probability
            distribution based on the statevector simulator.
        aux (dict, optional): A dictionary containing additional information
            about the circuit. Defaults to None.

    Returns:
        ProbDistribution: The probability distribution obtained from executing
        the circuit.
    """
    if aux is not None:
        multi = aux.get("multi")
        pm = aux.get("pm")
        # layouts = aux.get("layouts")
        if shots is None or shots == 0:
            multires = multirunV2(
                sampler=sampler,
                circuits=[circuit] * multi,
                list_parameter_values=[parameter_values] * multi,
                pm=pm,
                shots=None,
                combine=True,
                # layouts=layouts
            )
        else:
            multires = multirunV2(
                sampler=sampler,
                circuits=[circuit] * multi,
                list_parameter_values=[parameter_values] * multi,
                pm=pm,
                shots=shots // multi,
                combine=True,
                # layouts=layouts
            )
        pd = list(multires[0].data.items())[0][1].get_counts()
    else:
        pub = (circuit, parameter_values)
        if shots is None or shots == 0:
            # For use with statevector simulator
            job = sampler.run(pubs=[pub], shots=None)
        else:
            job = sampler.run(pubs=[pub], shots=shots)
            res = job.result()[0]  # PUBResult
            counts = list(res.data.items())[0][1].get_counts()
            pd = counts_to_pd(counts=counts)
    return pd


def get_cost_functionV2(cfg: dict, cost_history: list = None) -> callable:
    """
    Returns a function that computes the cost for a given set of parameters.

    Args:
        cfg (dict): A dictionary containing configuration settings for the
            cost function.
            The following keys are expected:
                - "cost_variant": The type of cost function to use, either
                    "TV" or "MMD".
                - "multi": Whether to run multiple instances of the circuit
                    and combine the results.
                - "sampler": The Qiskit sampler to use for executing the
                    circuits.
                - "circuit": The quantum circuit to optimize.
                - "prob_data": The data distribution to use for computing
                    the TV or MMD cost.
                - "mmd_instance": An instance of the MMD class to use for
                    computing the MMD cost.
                - "shots": The number of shots to take for each execution of
                    the circuit.
                - "maximmum": Maximal size of the represented number.
                - "layout_file": The file path to a JSON file containing the
                    layout information for the QPU.
                - "amsgrad": Whether to use the AMSGrad optimizer (True)
                    or not (False).

    Returns:
        callable: A function that computes the cost for a given set of
            parameters.
    """
    multi = cfg.get("multi")
    if multi:
        # layouts = readlayoutmap(
        #     filename=cfg.get("layout_file"),
        #     circuits=[cfg.get("circuit")] * multi
        #     )
        pm = cfg.get("pm")
        aux = {
            "multi": True,
            # "layouts": layouts,
            "pm": pm,
        }
    else:
        aux = None
    match cfg.get("cost_variant"):
        case "TV":
            cost_fun = get_cost_tvV2(
                p_data=cfg.get("prob_data"),
                sampler=cfg.get("sampler"),
                circuit=cfg.get("circuit").copy(),
                shots=cfg.get("shots"),
                maximmum=cfg.get("maximmum"),
                history=cost_history,
                aux=aux,
            )
        case "MMD":
            cost_fun = get_cost_mmdV2(
                p_data=cfg.get("prob_data"),
                mmd_instance=cfg.get("mmd_instance"),
                sampler=cfg.get("sampler"),
                circuit=cfg.get("circuit").copy(),
                shots=cfg.get("shots"),
                maximmum=cfg.get("maximmum"),
                history=cost_history,
                aux=aux,
            )
        case "MMD_torch":
            cost_fun = get_cost_mmd_torchV2(
                p_data=cfg.get("prob_data"),
                sampler=cfg.get("sampler"),
                circuit=cfg.get("circuit").copy(),
                shots=cfg.get("shots"),
                maximmum=cfg.get("maximmum"),
                history=cost_history,
                aux=aux,
            )
        case _:
            raise ValueError("Invalid cost variant.")
    return cost_fun


def get_cost_tvV2(
    p_data,
    sampler: SamplerV2,
    circuit: QuantumCircuit,
    shots: int,
    maximmum: int,
    history=None,
    aux=None,
) -> callable:
    """
    Returns the TV cost function for a given probability distribution.

    Args:
        p_data (ProbDistribution): The target probability distribution.
        sampler (SamplerV2): The Qiskit sampler to use for executing the
            circuit.
        circuit (QuantumCircuit): The quantum circuit to optimize.
        shots (int): The number of shots to take for each execution of the
            circuit.
        maximmum (int): Size of the maximal allowed number.
        history (list[float, ...], optional): A list of costs from previous
            iterations. Defaults to None.
        aux (dict, optional): A dictionary containing additional information
            about the circuit and its execution. Defaults to None.

    Returns:
        callable: The TV cost function for a given probability distribution.
    """

    def cost_tv_funV2(parameters):
        """Return TV cost for given parameters."""
        pd = get_pdV2(
            circuit=circuit,
            parameter_values=parameters,
            sampler=sampler,
            shots=shots,
            aux=aux,
        )
        prob = dist_pdf(dist=pd, maximmum=maximmum)
        cost = np.sum(np.abs(prob - p_data) ** 2)
        if history is not None:
            history.append(cost)
        return cost

    return cost_tv_funV2


def get_cost_mmdV2(
    p_data,
    mmd_instance,
    sampler: SamplerV2,
    circuit: QuantumCircuit,
    maximmum: int,
    shots: int,
    history=None,
    aux=None,
) -> callable:
    """
    Returns the MMD cost function for a given probability distribution.

    Args:
        p_data (ProbDistribution): The target probability distribution.
        mmd_instance (MMD): An instance of the MMD class to use for
            computing the MMD cost.
        sampler (SamplerV2): The Qiskit sampler to use for executing the
            circuit.
        circuit (QuantumCircuit): The quantum circuit to optimize.
        maximmum (int): Size of the maximal allowed number.
        shots (int): The number of shots to take for each execution of the
            circuit.
        history (list[float, ...], optional): A list of costs from previous
            iterations. Defaults to None.
        aux (dict, optional): A dictionary containing additional information
            about the circuit and its execution. Defaults to None.

    Returns:
        callable: The MMD cost function for a given probability distribution.
    """

    def cost_mmd_funV2(parameters):
        """Return MMD cost for given parameters."""
        pd = get_pdV2(
            circuit=circuit,
            parameter_values=parameters,
            sampler=sampler,
            shots=shots,
            aux=aux,
        )
        prob = dist_pdf(dist=pd, maximmum=maximmum)
        cost = mmd_instance.calc(prob, p_data)
        if history is not None:
            history.append(cost)
        return cost

    return cost_mmd_funV2


def get_cost_mmd_torchV2(
    p_data,
    sampler: SamplerV2,
    circuit: QuantumCircuit,
    maximmum: int,
    shots: int,
    kernel: str = "rbf",
    history: list = None,
    aux=None,
) -> callable:
    """
    Returns the MMD cost function for a given probability distribution.

    Args:
        p_data (ProbDistribution): The target probability distribution.
        sampler (SamplerV2): The Qiskit sampler to use for executing the
            circuit.
        circuit (QuantumCircuit): The quantum circuit to optimize.
        maximmum (int): Size of the maximal allowed number.
        shots (int): The number of shots to take for each execution of the
            circuit.
        kernel (str, optional): The type of kernel function to use in the MMD
            cost. Defaults to "rbf".
        history (list[float, ...], optional): A list of costs from previous
            iterations. Defaults to None.
        aux (dict, optional): A dictionary containing additional information
            about the circuit and its execution. Defaults to None.

    Returns:
        callable: The MMD cost function for a given probability distribution.
    """

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    def cost_mmd_fun(parameters) -> float:
        """Return MMD cost for given parameters."""
        pd = get_pdV2(
            circuit=circuit,
            parameter_values=parameters,
            sampler=sampler,
            shots=shots,
            aux=aux,
        )
        prob = dist_pdf(dist=pd, maximmum=maximmum)
        cost = float(
            torch_mmd(
                x=torch.tensor(prob[np.newaxis]).cuda(),
                y=torch.tensor(p_data[np.newaxis]).cuda(),
                kernel=kernel,
                device=device,
            )
        )
        if history is not None:
            history.append(cost)
        return cost

    return cost_mmd_fun


def get_gradient_analytic_function(
    cfg: dict,
    prob_history: list = None,
    grad_history: list = None,
    par_history: list = None,
) -> callable:
    """Get gradient function for the cost function specified in config file.

    Args:
        cfg (dict): Configuration dictionary containing all information about
            the cost function, including the cost variant and any additional
            parameters required by the cost function.
        prob_history (list, optional): A list to store the probability
            distributions calculated at each iteration of the optimization
            algorithm. Defaults to None.
        grad_history (list, optional): A list to store the gradients of the
            probability distributions calculated at each iteration of the
            optimization algorithm. Defaults to None.
        par_history (list, optional): A list to store the parameter values
            used to calculate the probability distributions and their
            gradients. Defaults to None.

    Returns:
        callable: A function that takes a list of parameters as input and
            returns the gradient of the cost function with respect to those
            parameters.
    """
    # resolve multishot and prepare aux_gradient dict
    multi = cfg.get("multi")
    if multi:
        layouts = readlayoutmap(
            filename=cfg.get("layout_file"),
            # circuits=config.get("circuits")
        )
        pm = cfg.get("pm")
        aux_gradient = {
            "multi": multi,
            "layouts": layouts,
            "pm": pm,
        }
    else:
        aux_gradient = None
    match cfg.get("cost_variant"):
        case "MMD_torch":
            gradient = get_gradient_analyticV2_torch(
                p_data=cfg.get("prob_data"),
                sampler=cfg.get("sampler"),
                num_qubits=cfg.get("num_qubits"),
                circuit=cfg.get("circuit"),
                shots=cfg.get("shots"),
                history_prob=prob_history,
                history_grad=grad_history,
                history_par=par_history,
                progress=True,
                aux=aux_gradient,
            )
        case "MMD":
            gradient = get_gradient_analyticV2(
                p_data=cfg.get("prob_data"),
                mmd_instance=cfg.get("mmd_instance"),
                sampler=cfg.get("sampler"),
                num_qubits=cfg.get("num_qubits"),
                circuit=cfg.get("circuit"),
                shots=cfg.get("shots"),
                history_prob=prob_history,
                history_grad=grad_history,
                history_par=par_history,
                progress=True,
                aux=aux_gradient,
            )
        case _:
            raise ValueError("cost variant not recognized")
    return gradient


def get_gradient_analyticV2(
    p_data,
    mmd_instance,
    sampler: SamplerV2,
    maximmum: int,
    circuit: QuantumCircuit,
    shots: int,
    history_prob: list = None,
    history_grad: list = None,
    history_par: list = None,
    progress=False,
    aux=None,
) -> callable:
    """Get gradient function for MMD cost with bound p_data, circuit, shots
        and history.

    Args:
        p_data (list): A list of probability distributions representing the
            data distribution.
        mmd_instance (MMD): An instance of the MMD class, which calculates
            the MMD between two probability distributions.
        sampler (SamplerV2): A sampler object used to generate probability
            distributions from quantum circuits.
        maximmum (int): The maximum value of the parameters for the
            distribution.
        circuit (QuantumCircuit): The quantum circuit that generates the
            probability distributions.
        shots (int, optional): The number of shots used to sample the
            probability distributions. Defaults to None.
        history_prob (list, optional): A list to store the probability
            distributions calculated at each iteration of the optimization
            algorithm. Defaults to None.
        history_grad (list, optional): A list to store the gradients of the
            probability distributions calculated at each iteration of the
            optimization algorithm. Defaults to None.
        history_par (list, optional): A list to store the parameter values
            used to calculate the probability distributions and their
            gradients. Defaults to None.
        progress (bool, optional): Whether to display a progress bar during
            the calculation of the gradient. Defaults to False.
        aux (dict, optional): Additional information required for the
            calculation of the gradient, such as layout maps or pulse mapping
            information. Defaults to None.

    Returns:
        callable: A function that takes a list of parameters as input and
        returns the gradient of the MMD cost with respect to those parameters.
    """

    def gradient_analytic(parameters) -> np.ndarray:
        """Analytic gradient function for parameter set."""
        # maximmum = int(num_qubits * '1', 2) + 1
        num_of_par = len(parameters)
        num_of_grad_points = 2 * num_of_par + 1
        # circuits_list = [circuit] * num_of_grad_points
        parameters_list = []
        gradient = []
        parameters_list.append(parameters.copy())
        for i in range(num_of_par):
            # pi/2 phase
            step = np.pi / 2
            pars_plus = parameters.copy()
            pars_plus[i] += step
            parameters_list.append(pars_plus)
            # -pi/2 phase
            pars_minus = parameters.copy()
            pars_minus[i] -= step
            parameters_list.append(pars_minus)
        pd_list = [
            dist_pdf(
                dist=get_pdV2(
                    circuit=c,
                    parameter_values=p,
                    sampler=sampler,
                    shots=shots,
                    aux=aux,
                ),
                maximmum=maximmum,
            )
            for c, p in zip([circuit] * len(parameters_list), parameters_list)
        ]
        prob = pd_list[0]
        for i in range(1, num_of_grad_points, 2):
            prob_plus = pd_list[i]
            prob_minus = pd_list[i + 1]
            grad_plus = mmd_instance.kernel_expect(
                px=prob, py=prob_plus
            ) - mmd_instance.kernel_expect(px=prob, py=prob_minus)
            grad_minus = mmd_instance.kernel_expect(
                px=p_data, py=prob_plus
            ) - mmd_instance.kernel_expect(px=p_data, py=prob_minus)
            gradient.append(grad_plus - grad_minus)
        if history_prob is not None:
            history_prob.append(prob)
        if history_grad is not None:
            history_grad.append(gradient)
        if history_par is not None:
            history_par.append(parameters)
        if progress:
            print(f"||Gradient|| = {np.linalg.norm(gradient)}")
        return np.array(gradient)

    return gradient_analytic


def get_gradient_analyticV2_torch(
    p_data,
    sampler: SamplerV2,
    maximmum: int,
    circuit: QuantumCircuit,
    shots: int,
    kernel: str = "rbf",
    history_prob: list = None,
    history_grad: list = None,
    history_par: list = None,
    progress=False,
    aux=None,
) -> callable:
    """Get gradient function for MMD cost with bound p_data, circuit, etc.
    Args:
        p_data (list): A list of probability distributions representing the
            data distribution.
        sampler (SamplerV2): A sampler object used to generate probability
            distributions from quantum circuits.
        maximmum (int): The maximum value of the parameters for the
            distribution.
        circuit (QuantumCircuit): The quantum circuit that generates the
            probability distributions.
        shots (int, optional): The number of shots used to sample the
            probability distributions. Defaults to None.
        kernel (str, optional): The kernel function used in the MMD
            calculation. Defaults to "rbf".
        history_prob (list, optional): A list to store the probability
            distributions calculated at each iteration of the optimization
            algorithm. Defaults to None.
        history_grad (list, optional): A list to store the gradients of the
            probability distributions calculated at each iteration of the
            optimization algorithm. Defaults to None.
        history_par (list, optional): A list to store the parameter values
            used to calculate the probability distributions and their
            gradients. Defaults to None.
        progress (bool, optional): Whether to display a progress bar during
            the calculation of the gradient. Defaults to False.
        aux (dict, optional): Additional information required for the
            calculation of the gradient, such as layout maps or pulse mapping
            information. Defaults to None.

    Returns:
        callable: A function that takes a list of parameters as input and
            returns the gradient of the MMD cost with respect to those
            parameters.
    """
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    def gradient_analytic(parameters):
        """Analytic gradient function for parameter set."""
        # maximmum = int(num_qubits * '1', 2) + 1
        num_of_par = len(parameters)
        num_of_grad_points = 2 * num_of_par + 1
        # circuits_list = [circuit] * num_of_grad_points
        parameters_list = []
        gradient = []
        parameters_list.append(parameters.copy())
        for i in range(num_of_par):
            # pi/2 phase
            step = np.pi / 2
            pars_plus = parameters.copy()
            pars_plus[i] += step
            parameters_list.append(pars_plus)
            # -pi/2 phase
            pars_minus = parameters.copy()
            pars_minus[i] -= step
            parameters_list.append(pars_minus)
        pd_list = [
            dist_pdf(
                dist=get_pdV2(
                    circuit=c,
                    parameter_values=p,
                    sampler=sampler,
                    shots=shots,
                    aux=aux,
                ),
                maximmum=maximmum,
            )
            for c, p in zip([circuit] * len(parameters_list), parameters_list)
        ]
        prob = pd_list[0]
        for i in range(1, num_of_grad_points, 2):
            prob_plus = pd_list[i]
            prob_minus = pd_list[i + 1]
            grad_plus = torch_mmd(
                x=torch.tensor(prob[np.newaxis]).cuda(),
                y=torch.tensor(prob_plus[np.newaxis]).cuda(),
                kernel=kernel,
                device=device,
            ) - torch_mmd(
                x=torch.tensor(prob[np.newaxis]).cuda(),
                y=torch.tensor(prob_minus[np.newaxis]).cuda(),
                kernel=kernel,
                device=device,
            )
            grad_minus = torch_mmd(
                x=torch.tensor(p_data[np.newaxis]).cuda(),
                y=torch.tensor(prob_plus[np.newaxis]).cuda(),
                kernel=kernel,
                device=device,
            ) - torch_mmd(
                x=torch.tensor(p_data[np.newaxis]).cuda(),
                y=torch.tensor(prob_minus[np.newaxis]).cuda(),
                kernel=kernel,
                device=device,
            )
            gradient.append(float(grad_plus - grad_minus))
        if history_prob is not None:
            history_prob.append(prob)
        if history_grad is not None:
            history_grad.append(gradient)
        if history_par is not None:
            history_par.append(parameters)
        if progress:
            print(f"||Gradient|| = {np.linalg.norm(gradient)}")
        return np.array(gradient)

    return gradient_analytic
