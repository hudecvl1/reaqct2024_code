"""Tools for QCBM training with COBYLA."""

import numpy as np
from costfun_tools import (
    get_init_params,
    init_tol,
    get_cost_functionV2,
)
from scipy.optimize import minimize
from qiskit_ibm_runtime import Sampler as IBMSampler
from qiskit_ibm_runtime import Session
import traceback

# TODO: Refactor train_cobyla to use cfg dict.


def train_cobylaV2(cob_cfg: dict) -> dict:
    """Train the parameters of a quantum circuit using COBYLA.

    Args:
        cob_cfg (dict): Configuration dictionary for training.

    Returns:
        dict: Dictionary containing the optimized parameters, the data history.
    """
    callback_dict = {"iters": 0, "par_hist": []}
    cost_history = []
    tol = init_tol(cfg=cob_cfg)
    parameters = get_init_params(cfg=cob_cfg)
    bounds = [(-np.pi, np.pi) for a in range(len(parameters))]

    def build_callback(callback_dict):
        """Build callback function for Scipy COBYLA."""

        def callback(current_vector):
            callback_dict["iters"] += 1
            callback_dict["par_hist"].append(current_vector)

        return callback

    callback = build_callback(callback_dict)

    # get cost function from costfun_tools
    cost_fun = get_cost_functionV2(cfg=cob_cfg)

    # run COBYLA optimization
    optimizer_result = minimize(
        fun=cost_fun,
        x0=parameters,
        method="cobyla",
        callback=callback,
        bounds=bounds,
        tol=tol,
        options={"maxiter": cob_cfg["max_iter"], "rhobeg": cob_cfg["rhobeg"]},
    )

    # return optimizer_result, cost_history, callback_dict
    res_dict = {
        "x": optimizer_result.x,
        "fun": [optimizer_result.fun],
        "nfev": [optimizer_result.nfev],
        "iters": [callback_dict["iters"]],
        "par_hist": callback_dict["par_hist"],
        "cost_history": cost_history,
    }
    return res_dict


def train_cobyla_ibmV2(cob_cfg: dict) -> dict:
    callback_dict = {"iters": 0, "par_hist": []}

    def build_callback(callback_dict):
        """Build callback function for Scipy COBYLA."""

        def callback(current_vector):
            callback_dict["iters"] += 1
            it = callback_dict["iters"]
            if it % 10 == 0:
                print(f"Iteration: {it}.")
            callback_dict["par_hist"].append(current_vector)

        return callback

    cost_history = []
    tol = init_tol(cfg=cob_cfg)
    parameters = get_init_params(cfg=cob_cfg)
    callback = build_callback(callback_dict)
    service = cob_cfg["service"]
    backend = cob_cfg["backend"]
    max_iter = cob_cfg["max_iter"]
    rhobeg = cob_cfg["rhobeg"]
    options = cob_cfg["options"]

    print(f"service: {service}")
    print(f"backend: {backend}")
    with Session(service, backend=backend) as session:
        # COBYLA is not parallel => use Session instead of Batch.
        print(f"session: {session}")
        try:
            ibmsampler = IBMSampler(session=session, options=options)
            print(f"sampler: {ibmsampler}")
            # get cost function from costfun_tools
            cost_fun = get_cost_functionV2(cfg=cob_cfg)
            # run COBYLA minimization
            bounds = [(-np.pi, np.pi) for a in range(len(parameters))]
            optimizer_result = minimize(
                fun=cost_fun,
                x0=parameters,
                method="cobyla",
                callback=callback,
                bounds=bounds,
                tol=tol,
                options={"maxiter": max_iter, "rhobeg": rhobeg},
            )
        except Exception as e:
            # close session on failure
            print(f"Failed: {e}")
            print(traceback.format_exc())
            session.cancel()
        # close session
        session.close()
    # return optimizer_result, cost_history, callback_dict
    res_dict = {
        "x": optimizer_result.x,  # Solution parameters, [float, ...]
        "fun": [optimizer_result.fun],  # Solution cost, float
        "nfev": [optimizer_result.nfev],  # Number of calls to cost fun, int
        "iters": [callback_dict["iters"]],  # Number of iterations, int
        "par_hist": callback_dict["par_hist"],
        "cost_history": cost_history,
    }
    return res_dict
